\documentclass[11pt]{ltxdoc}

\newcounter{magicrownumbers}
\newcommand\rownumber{\stepcounter{magicrownumbers}\arabic{magicrownumbers}}
\usepackage{geometry}
\geometry{a4paper, hmargin=2cm, vmargin=2cm}
\usepackage{graphicx}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{tabularx}
\usepackage[table]{xcolor}
\usepackage{color}
\usepackage{hyperref}
\usepackage[superscript,biblabel]{cite}
\usepackage[font=scriptsize,width=14cm]{caption}
\usepackage{wrapfig}
\usepackage{times}
\usepackage{float}
\usepackage{fancyhdr}

\usepackage[compact]{titlesec}
\usepackage[parfill]{parskip}
\usepackage{enumitem}


\pagestyle{fancy}
\fancyhf{}
\fancyhead[LE,RO]{NanoChaT}
\fancyhead[RE,LO]{Center for Nanoscale Charge Transport}
\fancyfoot[C]{\thepage}

\makeatletter
\newcommand\footnoteref[1]{\protected@xdef\@thefnmark{\ref{#1}}\@footnotemark}
\makeatother

% -- main purpose --

\begin{document}
%%%%
%% Main document 12pt or {\large ...}
%%%%
{\large 
\begin{center}
\huge{\bf Center for Nanoscale Charge Transport (NanoChaT)}\\
\begin{center}
    \includegraphics[height=.2 \textwidth]{logoSFF}
\end{center}
\end{center}
\thispagestyle{empty}
\noindent
{\bf Project summary} \\
 The transition to a green society requires the development of improved
technologies for efficient utilization of solar energy. To enable
these improvements, powerful modeling approaches are needed to
understand the various steps in the conversion of photons into fuel,
either as electrons, ions or energy carrying molecules. In general the
steps can be described as:

\begin{enumerate}
\item Excitation
\item Charge separation
\item Transport
\item Conversion
\end{enumerate}

where \textbf{charge transport} is central to the
process. Unfortunately, no single modeling approach is able to give us
a better understanding of charge transport at the nanoscale. What is
needed is a research infrastructure which combines skills and
knowledge within such diverse areas as continuum modeling,
chemometrics, chemoinformatics, non-equilibrium thermodynamics,
atomistic simulations, and electronic structure theory. The NanoChaT
center of excellence has the unique feature of providing such a
infrastructure for cross-disciplinary research where complementary
knowledge and skills are used to address problems at each step in the
solar energy conversion process.

For instance, the photo excitation process observed in semiconductors
can be studied by either density functional theory or more accurate
wave function theory. Charge separation may also be described by both
chemoinformatics and molecular dynamics. While electron transport is
typically being studied by quantum chemistry methods, ion transport
can be studied by continuum models, thermodynamics or nanoscale
molecular simulations. In addition, we will use multi-level methods
and chemometrics/machine learning techniques to connect the different
length- and time-scales, especially at the nanoscale where electron-
and ion-transport influence each other such as in electrode
reactions. Our generic approach will be directed towards these
selected number of applications like organic solar cells,
supercapacitors, batteries, fuel cells, thermoelectric energy
convertors, and artificial photosynthesis.  We have the ambitious goal
to establish breakthroughs in each of the process steps mentioned
above.
%PO: include a sentence on coupling to heat transfer and applied voltages/electric field
\\ \newline

\noindent
{\bf Primary and secondary objectives of the project} \\
Our primary objective is to develop a generic multi-scale approach to
model all aspects of charge transport at the nanoscale involving both
electron- and ion-transport. This model will not be just descriptive,
but should increase our understanding on this process such that it can
generate an avalanche of industrial break-throughs. We will steer our
theoretical and computational model development in order to address
relevant unresolved questions with respect to specific applications
like organic solar cells and supercapacitors.  If successful, our
project will be a unique realization of a multi-scale approach ranging
from highly accurate quantum mechanical calculations up to the
macroscopic limit. Our secondary objectives are related to the
applications. Our modeling will pave the way for the development of
alternative materials for environmental energy production, or will
hint on experimental strategies to increase the efficiency or decrease
aging of present energy devices.

\clearpage
\noindent


{\Large \bf Proposed research} \\
Charge transport at the nanoscale is a fundamental key process in
several nanodevices which play an increasingly important role on the
world energy balance.\footnote{E. Pop, Energy Dissipation and
  Transport in Nanoscale Devices, {\em Nano Res} 3, 147-169, (2010).}
Nanotechnology is crucial for the high efficiency conversion of solar
energy or use of solar energy for hydrogen production, and it will be
central in future possibilities for storage and transmission of
energy.  The theoretical research on nanoscale processes is making
rapid development, also because the field contains many unexplored
opportunities while the phenomena at this scale is difficult to
address with standard theories.  Theories for describing nanosized
objects fall just in between the more established theories for the
atomistic and continuum regions.  Finite size effects play an
important role implying that successful approximations like the use of
periodic boundary conditions or analytical tricks based on the
translational symmetry can't and shouldn't be applied; the macroscopic
limit does not apply.  On the other hand, the nanoscale exceeds the
atomistic and molecular length scale which can be treated by highly
accurate quantum chemistry methodologies.  At the nanoscale the
quantum effects are important, but often not limited to a small
atomistic group. The cooperative effects due to the interactions of
multiple atoms sometimes cause unexpected
phenomena\footnote{J. P. Bergfield, M. A. Ratner, C. A. Stafford, and
  M. Di Ventra, Tunable quantum temperature oscillations in graphene
  nanostructures, {\em Phys. Rev. B}, 91, 125407, (2015)}. In
addition, when a macroscopic number of nanoparticles interacts also
continuum descriptions become essential.  To connect all these length
scales in a theoretical framework is essential since the beauty of
nanotechnology shows that quantum mechanical phenomena at the
nanoscale can have an effect at the macroscopic scale.  Especially
regarding nanoscale charge transport there are yet unexplored avenues
which lack a solid theoretical or computational treatment. The
development of such a combined theoretical-computational framework
could have an enormous impact regarding applications just like the
theory of semiconductors have enabled the creation of the transistor
which has revolutionized the field of electronics.

Nanoscale charge transport is especially intriguing from a theoretical
point of view when both electron and ion transport occur
simultaneously and mutually influence each other. The description of
such effects requires the combination of different disciplines which
are seldom available within a single research collaboration. The
availability of a generic theory or computational method will be
invaluable to unravel the detailed functioning of capacitors,
batteries, solar cells, electrochemical convertors, and other energy
related nanodevices. Another complexity not mentioned above is that
the dynamical nature of transport processes brings in another
dimension which is time. This implies that besides linking the
different length scales, our method should also be able to link
different time scales. For instance if diffusion occurs via the
crossing of different barriers or if the process involves chemical
reactions, there can be a huge range of relevant timescales ranging
from molecular motion to the barrier crossing
event\footnote{C.~Dellago, P.~G. Bolhuis, F.~S. Csajka, and
  D.~Chandler, Transition path sampling and the calculation of rate
  constants, {\em J. Chem. Phys.}, 108:1964--1977, (1998).}.
In conclusion, the topic of charge transport is timely, highly challenging, but also highly promising since it may open new fields of energy related applications.\\

\noindent {\bf Applications and Workpackages.} The research work will
be structured in four work-packages (WPs) which are categorized into
Excitation (1), Charge Separation (2), Transport (3) and Conversion
(4). However, the methodology we will develop will stretch across all
four WPs. The WPLs are selected based on their track record in the
specific application field. However, the research within the WP is not
a separate entity dictated by the WPL's research interest. Rather, the
WPL will coordinate the collaboration between the different
researchers and ensure that the cooperative strategy will be applied
to the application field.  The WPs listed below involve both electron-
and ion-transport, and a more fundamental understanding on these
phenomena at the nanoscale is expected to create breakthroughs
regarding efficiency and new functionality. The consortium, presented
here, can provide an unique complementary set of methods and theories
which are able to bridge the relevant length- and time-scales and to
connect physically distinct disciplines related to ion-transport on
one side and electron transport on the other side. This proposal is
going to establish that. We give a sketch of our overall
methodological strategic plan in the next paragraph.

At the atomistic scale the highly accurate Coupled Cluster (CC)
framework of models is at our disposal. The CC methods are
computational intensive due to their steep scaling with the size of
the system, but the applicability range can be extended by the
recently developed multi-level CC methods\footnote{R.~H. Myhre,
  A.~M. J. Sanchez de Meras, and Henrik Koch, Multi-level coupled
  cluster theory, {\em J. Chem. Phys.},141, 224105 (2014).}. However,
the CC methods will be intractable at some point when increasing the
length scale, at which point we will use density functional theory
(DFT).  The choice of density functional is generally based on common
practice rather than system specific benchmarking which is likely to
cause a significant drop in accuracy compared to CC. However, we can
use techniques from chemometrics to select the right functionals to
maintain accuracy and predictive power at the larger length
scale\footnote{V. Venkatraman, S. Abburu, B. K. Alsberg, Can
  chemometrics be used to guide the selection of suitable DFT
  functionals?, {\em Chemom. Intell. Lab. Syst.} 142, 87-94 (2015).}.
Also the timescale can be extended, even without invoking
approximations using Transition Interface Sampling
(TIS)\footnote{\label{titus_foot}T. S. van Erp, D.~Moroni, and
  P.~G. Bolhuis, A novel path sampling method for the sampling of rate
  constants, {\em J. Chem. Phys.}, 118:7762-7774, (2003).}.  This is
required if there is for instance a chemical reaction with a high
barrier.  This approach also provides a way to connect classical
dynamics with quantum-based simulations via the new QuanTIS
method\footnote{\label{anders_foot} A. Lervik and T. S. van Erp,
  Transition path sampling and the calculation of rate constants,{\em
    J. Chem. Theory Comput.} 11: 2440-2450 (2015).}. The method
establishes an dynamical analogue of QM/MM in which two levels of
theory are connected in time rather than space. In addition, the
method\footnoteref{anders_foot} can also be used to fit parameters of
reactive force fields\footnote{H. Y. Cheng, Y. A.  Zhu, D. Chen,
  P. O. Astrand, P. Li, Z. W. Qi, and X.G. Zhou, Evolution of Carbon
  Nanofiber-Supported Pt Nanoparticles of Different Particle Sizes: A
  Molecular Dynamics Study, {\em J. Phys. Chem. C} 118, 23711-23722
  (2014).} on-the-fly (e.g using simulated annealing or evolutionary
algorithms\footnote{\label{alsberg_foot}V. Venkatraman, M. Foscato,
  V. R. Jensen, and B. K. Alsberg, Evolutionary de novo design of
  phenothiazine derivatives for dye-sensitized solar cells, {\emph
    J. Mater. Chem. A} 3, 9851-9860 (2015).}).  In this way we can
move from the atomistic and electronic motions to the processes at the
nanoscale.  This will allow us to study electron transfer at the
electrodes in combination with ion diffusion. In order to study the
relation between the nanoscale processes and the macroscopic processes
defining e.g. the efficiency of an Li-ion battery, we will also
connect these simulations with non-equilibrium
thermodynamics\footnote{S. Kjelstrup, P. J. S. Vie, L. Akyalcin, P
  Zefaniya, J. G. Pharoah, and O. S.  Burheim, The Seebeck coefficient
  and the Peltier effect in a polymer electrolyte membrane cell with
  two hydrogen electrodes, {\em Electrochim.  Acta} 99, 166-175
  (2013).}  and continuum models of charge
transport\footnote{B. A. Grimes, and A. L. Liapis, The Interplay of
  Diffusional and Electrophoretic Transport Mechanisms of Charged
  Solutes in the Liquid Film Surrounding Charged Nonporous Adsorbent
  Particles Employed in Finite Bath Adsorption Systems, {\em
    J. Colloid Interf. Sci.}, 248, 504-520 (2002).}.  For instance
non-equilibrium molecular dynamics\footnote{F. Roemer, A. Lervik,
  F. Bresme, Nonequilibrium molecular dynamics simulations of the
  thermal conductivity of water: A systematic investigation of the
  SPC/E and TIP4P/2005 models, {\em J. Chem.  Phys. }  137, 074503
  (2012).} can be used to match parameters of non-equilibrium square
gradient theory for the description of mass and heat transfer at
nano-objects\footnote{\O. Wilhelmsen, T. T. Trinh, S. Kjelstrup,
  T. S. van Erp, and D. Bedeaux, Heat and Mass Transfer across
  Interfaces in Complex Nanogeometries, {\em Phys. Rev. Lett. }  114,
  065901 (2015).}.  This type of multi-scale approach will be applied
to all WPs. Though each WPs has specific modeling problems and
objectives that we will discuss in the following.
%Suggestion of Odne. To make them believe that we will actually collaborate together instead of being inside our own little islands doing 
%our own things
%\emph{Quarterly meetings will be held with all participants outside the university campus in order to stimulate collaborative efforts between researchers from different disciplines.}
 
%{\bf Some words on multi-level methods, QuanTIS, force field development etc}

\textbf{WP1:  Excitation: Koch/Åstrand}\\


\textbf{WP2:  Charge separation:  Alsberg/van Erp.}\\
%PO Organic solar cells and DSSC are not the same thing!

A key aspect of solar cell functioning is the separation and
collection of charge with a minimum of recombination. Of particular
interest to the center is the study of non-Silicon based solar cells
such as dye-sensitized solar cells (DSSCs), bulk heterojunction organic
solar cells (BHJ) or nanoparticle solar cells. High efficiency charge
separation is achieved by mainly two effects: 1) exploitation of
forward directed kinetics at interfaces and 2) utilization of internal
electrical fields caused by a built-in voltage and by the distribution
of photogenerated charges.

The first step in the process is an effective separation of the
electron hole pair (the exciton) formed by the excitation dipole where
the photogeneration event takes place. The second step involves the
central layer which contains an absorber material and two extracting
contacts. This layer allows an efficient separation of the
electron-hole pair from where they are generated to their respective
contacts to ensure a net current flow. This layer consist of single or
multiple layers. In DSSCs, the sensitizer molecule is positioned
between an electron transport medium (nanostructured TiO$_2$) and a
hole transport layer (either a redox electrolyte or a solid hole
conductor). Here, charge extraction competes with the internal
recombination of the electron hole pair. Thus, fast extraction is
important to ensure efficient charge separation. Although,
recombination of the primary electron–hole pair is usually not an
issue in DSSCs using metal-based dyes, for organic dyes it becomes a
concern.
% light absorber (electron donor) and the organic electron transporter
% (electron acceptor), usually a functionalized fullerene.
Central to improving the performance of DSSCs is the design of better
dye molecules, in particular metal-free organic dyes.  In a recent
article\footnoteref{alsberg_foot} evolutionary {\em de novo} design
was presented, wherein molecular structures were systematically
assembled and iteratively refined in a computer according to the
principles of evolution.  Central to this scheme is the computation of
the power conversion efficiency (PCE) of the solar cell which is to be
optimised. Owing to the complex and multiscale nature of the device
morphology, the PCE (and other properties) cannot as yet be computed
directly using quantum chemistry methods. Instead, the prediction of
PCE and other property values is performed using machine learning
models based on data taken from either the literature, provided by
experimental collaborators or from costly first principles
computations. However, relying on literature experimental data to
create models to screen for promising dye molecules is not optimal,
given the inter-laboratory variability of measurements and
protocols. Thus, the center will also focus on improving ways to
compute important properties correlated with the PCE such as
recombination probability, electron injection efficiency and dye
aggregation using a variety of computational methods at our disposal.
% The PCE may be reduced by various physical phenomena in the solar
% cell such as increased . This reduces the PCE due to intermolecular
% excited state quenching.
For instance, dye aggregation which involves the interaction of dye
molecules bonded to the semiconductor surface (such as TiO$_2$) is
known to reduce the PCE due to quenching of intermolecular excited
states. Aggregation can be studied by molecular dynamics simulations
using either force fields or DFT methods. The aim of the simulations
will be to understand and predict the degree of dye aggregation to
accomplish efficient screening of suitable DSSC dyes. The output from
these simulations can be used as fitness measures in an evolutionary
{\em de novo} design to propose dye molecules which show a minimum of
aggregation. The use of machine learning to correlate molecular
structure to predicted molecular dynamics properties can be used to
create fast fitness functions which will speed-up the search for
improved structures. This shows how machine learning, molecular
dynamics, DFT and evolutionary {\em de novo} design together are used
toobtain results that separately would not solve a problem. 

Fast charge separation and prevention of recombination are also
important issues in polymer solar cells (PSC). These devices are
comprised of a polymer as an electron donor (D) and fullerene
(typically a soluble derivative of C60 or C70) as an electron acceptor
(A). On photon absorption, excitons are generated and start diffusing
towards the D-A interface. The bound excitons dissociate at the D-A
interface to form a geminate electron-hole pair. The separated hole
and electron are then transported through the donor and acceptor
materials, respectively to the appropriate contacts. % For efficient
% charge generation, the excitons must be produced within a diffusion
% length (LD) of the nearest D-A interface, which can be satisfied using
% a bulk heterojunction (BHJ) solar cell architecture in which the donor
% and acceptor materials are mechanically blended together. The energy
% difference between the HOMO level of the donor and the LUMO level of
% the acceptor is the primary driving force for dissociation of the
% exciton. 
In contrast to the DSSCs, PSCs are somewhat less dependent on the
structure of the polymer for achieving high PCE values. Critical
values of how the donor and acceptor materials are mechanically
blended together (i.e. the morphology) and other experimental solar
cell parameters become more important to optimizing the PCE. For both
PSCs and DSSCs \textbf{stability} also becomes an important issue
which must be addressed. However, it is not straightforward to
calculate from first principles all the complex phenomena defined
under the concept of ``stability''. Thus, simplied measures correlated
with the real stability must be used. Machine learning, DFT and
molecular dynamics will be employed to model thermal conductivity,
glass transition temperature (Tg), and the temperature of half
decomposition. In addition, various electrolyte interactions in DSSCs
with the semiconductor, dye and electrodes that are related to the
solar cell stability will also be studied.




%\item[WP1:] {\bf Organic solar cells.} WPL:  Alsberg/van Erp.\\
% There is an intense research activity worldwide to develop
%  alternatives to silicon-based solar technologies. One of the most
%  promising candidates is dye-sensitized solar cells (DSSCs, also
%  termed Grätzel cells) which are much cheaper to manufacture, have a
%  smaller carbon footprint and are more flexible than silicon-based
%  solar cells. However, their solar-to-electrical energy conversion
%  efficiency seems to have stagnated at 10–12\%, compared with up to
%  25\% for crystalline-silicon devices. Central to improving the
%  performance of DSSCs is the design of better dye molecules, in
%  particular metal-free organic dyes.  In a recent
%  article~\cite{Alsberg3}
%  from the Alsberg group at ATK, a new approach based on {\em in
%    silico} evolutionary {\em de novo} design was presented, wherein
%  molecular structures were systematically assembled and iteratively
%  refined in a computer according to the principles of evolution.
%  Central to this scheme is the computation of the power conversion
%  efficiency (PCE) which is to be optimised. Other properties of the
%  dye molecules are also of interest such as long term stability,
%  toxicity and various optical properties. Owing to the complex and
%  multiscale nature of the device morphology, the PCE (and other
%  properties) cannot as yet be computed directly using quantum
%  chemistry methods. For those properties which can be computed, the
%  computational cost may be too high to be used for high throughput
%  screening which is needed in an evolutionary {\em de novo} design
%  approach where hundreds or thousands of structures are
%  examined. Thus, an important alternative approach being used is
%  employing a quantitative structure-property relationship (QSPR)
%  model which is built using data taken from either the literature,
%  provided by experimental collaborators or from costly
%  computations.
%  % A predictive machine learning/chemometric model is
%  %created that correlates computationally inexpensive structure
%  %descriptors calculated for each dye (such as computed vibrational
% % frequencies and molecular orbital energies) and the reported
%  %property to be optimised (such as the PCE or the stability). 
%  The
%  established QSPR model is exploited in the evolutionary setup to
%  steer the molecular design towards novel structures with
%    improved property values, while excluding unpromising ones.
%In cases of insufficient experimental data for
%  building predictive QSPR models, investigations will be more reliant
%  on theoretical calculations. Of particular interest is to explore
%  whether the our evolutionary {\em de novo} design method can use
%  fitness values based on results from quantum chemical computations of 
%  the photoexcited dynamics of dye molecules.
%


  \textbf{WP2: { Chemical reactions in Electric fields.} WPL: \AA strand/Grimes.}\\
  Many chemical processes of relevance for this project occur in high
  electric fields (in an applied voltage) as for example in batteries,
  solar cells, fuel cells and electrically insulating
  materials. Although in principle the same computational methods
  apply as for the isolated system, additional challenges arise for
  modeling chemical processes in an electric field. For example, the
  electronic ground state may not be the same anymore and the
  ionization energy is much lower in an electric field limiting the
  number of available excited states. In addition, energy is
  continuously added to a system in an applied voltage, which has to
  be balanced by emission of heat or light to reach a steady-state
  condition, so equilibrium methods do not apply for a system in an
  electric field. In this WP, we will validate and develop molecular
  modeling tools applicable for studying molecular systems in an
  electric field including quantum chemical response properties as
  excitation energies, electron transport in a dielectric material as
  well as ion transport in condensed media.
 

\textbf{WP3: {\bf Photochemical energy conversion} WPL: H\o yvik/Koch}\\
Molecules can convert sunlight into different forms of energy, for example changing chemical bonds, or converting the absorbed energy to heat without molecular degradation, e.g.,  through a concerted motion of electrons and nuclei in the excited state. Such processes are highly relevant to be able to exploit photochemical energy conversion, but the processes are challenging to understand from experiments alone. Per today, methods which provides essential accuracy for molecular structure and excitation energies, fail to describe photoexcited dynamics. Further, the optimal theoretical framework for describing excited state properties assumes that the molecule is initially in the ground state. In this WP, we will develop new theoretical models, in the coupled cluster framework, that enables a description of photoexcited dynamics and properties of molecules in excited states. Some of the excited state properties are local in nature, and for these methods we will also implement the developed theory in a multi-level framework, to obtain computational savings. The WP will be carried out in collaboration with experimentalist at SLAC National Accelerator Laboratory at Stanford University. 




\textbf{WP4: {\bf Electrochemical energy conversion.} WPL: Burheim/Kjelstrup} \\
Electrical energy is momentaneous and requires imediate consumption or storage. A society founded on renewable energy technology will be a heavy user of Li-ion batteries, fuel cells, thermoelectric converters, 
supercapacitors, and advanced flow-cell batteries - simply because fossile energy (chemical) currently constituting 80+\% is mainly replaced by electrical energy (PV and wind).

Numerous works have been written on red/ox reactions, but a microscopic picture on how and under which conditions the electron and ion separate or merge at the electrode has, however, not been established\footnote{P. Ballone and   R. Cortes-Huerto, Ab initio simulations of thermal decomposition and of electron transfer reactions in room temperature ionic liquids, {\em Faraday Discuss} 154, 373-389 (2012).}. The conditions of the electrode surface (its polarization, temperature, excess concentration, shape) is critical for a high efficiency system, and may decide the mechanism.    
Currently, all of them are dealing with electrode potential jump at isothermal conditions. 
Kjelstrup and Bedeaux predicted in 2008\footnote{D. Bedeaux and S. Kjelstrup, The measurable heat flux that accompanies active transport by Ca$^{2+}$-ATPase, {\em  Phys. Chem. Chem. Phys},10, 48, 7304-7317 (2008).} 
that a local thermal driving force can also drive the electrochemical reaction. % rate. 
In order to verify 
this prediction with 
molecular simulations,
we will develop rare-event simulation algorithms\footnoteref{titus_foot} for non-equilibrium phenomena at the electrode surface
in collaboration with WP2.
New non-equilibrium algorithms\footnote{S. R. Williams,  Rare-event sampling for driven inertial systems via the nonequilibrium distribution function, {\em Phys. Rev. E} 88,043301, (2013).} can %open up for 
bring a whole new understanding of the coupling that is essential for energy conversion. Simulation methods can give a better understanding of the local electric potential- and concentration profiles in these systems. 
This will give us a 
handle on the energy conversion in these non-isothermal cells, 
which will 
help define new experiments, and eventually lead to improved production methods and use of these devices. Moreover, quantum modelling is essential to understand how partial oxidation numbers appears as gradients in electric fields in battery cathodes where mixed delithiated mixed metal (Mn, Fe, Co, Ni) oxides constitute the Li-ion battery cathodes. This has become an emerging field as the requirements to charging time for battery electric public transport is put to an extreme. Modelling these strucural-, charge- and thermal effects effects on a quantum level is a field where Koch, Høyvik and Åstrøm have track records. Reproducing this on an experimental level as well as in more continuum oriented models is where Kjelstrup, Burheim and Grimes holds a tack record.  
Examples are symmetry cells which consist of two cathodes at different temperatures. 
With an exceptional basis in non-equilibrium thermodynamics the WP leaders are well set to find and model the transported entropy and reversible heat changes, properties which are decisive in this context. 
In order to engineer new materials for longer device time the reversible heat terms as well as entropy production need be addressed by combining these methods.
As a consequence of the introduction of renewable energy, novel electrochemical energy storage devices are currently and continuously proposed.\newline






\noindent
{\Large \bf Organization of the center} \\ 
There are eight scientists in the NanoChaT collaboration where seven are employed at NTNU and one at HiST. In 2016 the two universities will merge and all members will relocate to the 
CoE's common physical premises at the Department of Chemistry (IKJ) at the Faculty of Science and Technology (NT-faculty). As detailed in Table 1, the group covers a broad range of competences in theoretical and computational chemistry, as well as experimental 
electrochemical energy conversion. We give priority to early career researchers 
when selecting the six (the upper limit) principal investigators. This will provide Burheim and H\o yvik with unique career opportunities as WP leaders and they will be assisted by Kjelstrup and Koch.



\begin{table}[H]
\centering
\caption{The NanoChaT collaboration.}
\includegraphics[width=14cm]{table_grey2.png}
\end{table}

\noindent
A management team, consisting of the Director and Vice Director together with IKJ Head of Department Marie-Laure Olivier 
as administrative leader,  is appointed and will be responsible for the daily operation of the center.  An administrative secretary will be employed by the center whom will participate in the leader team meetings. The Vice Director acts as advisor to the Director on a daily basis, and takes on the responsibilities of Director in the absence of the latter. 
Director and Vice Director will switch roles after the 5 year mid-term of the CoE.
A council of all investigators will meet once a week to discuss and plan research projects. 

\noindent
The CoE will have an Executive Board which oversees that the CoE is operating according to its mandate. The Executive Board will also acts as an ambassador for the CoE, and is responsible with respect to the institutions financing it: NTNU and RCN. The Director acts as secretary, and does not have voting rights on the Executive Board. The Dean of the NT-faculty (Prof. Anne Borg) is chairman of the Board. Further, the CoE will have a  Scientific Advisory Board with  members composed of academics (primarily experimentalists), researchers and industrial representatives who have interest in the research topics of the CoE and understand the practical benefits of the results produced by the CoE. The detailed composition of the Boards will given in phase 2.


The Scientific Advisory Board members will be invited once a year to CoE seminars  to review the current status of the research and offer advice and opinions on how to guide the fundamental research moving forward based on what they perceive as practical benefits of the CoE's research.
The Scientific Advisory Board reports to the Executive Board on the scientific work and output of the CoE. It meets once a year with the CoE and on the basis of this meeting writes a report to the Executive Board. The report concludes with recommendations on the scientific direction of the CoE. 

%People that we will invite for the Advisory Board and have confirmed their interest are Assoc. Prof. B\aa rd Hoff, Assoc. Prof. Odd Gautun, and  Prof. Anne Fiksdahl(organic chemistry, IKJ), Preben Vie (IFE, Institute for Energy Technology), Oddmund Mallevik (former employee Statoil, division electrolysis ??).

\noindent
To strengthen the embedding of our research within international collaborations 
we will open five Professor II positions (each with a duration of 4 years) which will complement the three existing Professor II's  associated with the center (see Table \ref{profII}). This will give added value of creating the CoE, since the Professor II's will benefit NTNU and Norway through their international networks and competences. The CoE will also give added value in terms of 
\begin{itemize}
\item training highly competent candidates for the industry, academia, or public sector.
\item generating a platform of knowledge on charge transport processes on a multi-level scale which stretches across several disciplines at the NT-faculty and which integrates researchers at the NTNU fusion partner HiST.
\item motivating students by creating a visible and strong link between courses in disciplines involved in the CoE (quantum chemistry, thermodynamics, molecular modeling, transport processes, electrochemistry).
\end{itemize}


\begin{table}[H]
\centering
\caption{\label{profII} Overview of prospective and current (marked with an asterisk) Professor II's.}
\includegraphics[width=14cm]{assoc_inv.png}
\end{table}

\noindent
When consolidating scientists from several different research fields, it is important to achieve good communication across disciplines. This is needed 
to establish the cross-links between the different methodologies and to establish the feeling to be part of a team. Therefore, CoE seminars will be organized four times per year. At least two of these four involve overnight stays off campus  to increase the coherency of the CoE team and to enforce 
the bridge building and brain storming sessions without the daily duties and distractions  that all researchers 
experience in their normal work environment. 
The CoE seminars contain presentations by PhD students and postdocs showing their latest preliminary results, prospectives, and potential problems and obstacles that need to be overcome. PI's will present more overview talks and long-term plans. In addition, their should be ample time for discussions.


 \end{document}





%Old WP before change Odne
\textbf{WP4: {\bf Electrochemical energy conversion.} WPL: Burheim/Kjelstrup} \\
A society using renewable energy technology will be a heavy user of Li-ion batteries, fuel cells, thermoelectric converters, 
%reverse electrodialysis cell, in short; electrochemically converting systems. 
supercapacitors, and advanced flow-cell batteries.
Numerous works have been written on red/ox reactions, 
but a microscopic picture on how and under which conditions the electron and ion separate or merge at the electrode has, however, not been established\footnote{P. Ballone and   R. Cortes-Huerto, Ab initio simulations of thermal decomposition and of electron transfer reactions in room temperature ionic liquids, {\em Faraday Discuss} 154, 373-389 (2012).}. The conditions of the electrode surface (its polarization, temperature, excess concentration, shape) is critical for a high efficiency system, and may decide the mechanism.    
Currently, all of them are dealing with electrode potential jump at isothermal conditions. 
Kjelstrup and Bedeaux predicted in 2008\footnote{D. Bedeaux and S. Kjelstrup, The measurable heat flux that accompanies active transport by Ca$^{2+}$-ATPase, {\em  Phys. Chem. Chem. Phys},10, 48, 7304-7317 (2008).} 
%from the form of the excess entropy production of the interface, 
that a local thermal driving force can also drive the electrochemical reaction. % rate. 
%The theoretical prediction was verified by another non-linear theory (GENERIC) in 2014~\cite{BD1}. 
In order to verify 
%The aim is now to verify the 
this prediction with %advanced 
%computational techniques. 
molecular simulations,
we will develop rare-event simulation algorithms\footnoteref{titus_foot} for non-equilibrium phenomena at the electrode surface %, choosing particular model systems, also 
in collaboration with WP2. %Present approaches do not provide the right reaction path, and it is an aim to reach such. 
New non-equilibrium algorithms\footnote{S. R. Williams,  Rare-event sampling for driven inertial systems via the nonequilibrium distribution function, {\em Phys. Rev. E} 88,043301, (2013).} can %open up for 
bring a whole new understanding of the coupling that is essential for energy conversion. Simulation methods can give a better understanding of the local electric potential- and concentration profiles in these systems. 
This will give us a 
%To have a better founded 
handle on the energy conversion in these non-isothermal cells, 
%will next benefit our experimental program, 
which will 
help define new experiments, and eventually lead to improved production methods and use of these devices. Examples are symmetry cells
which consist of two cathodes at different temperatures. 
%As special class of experimental systems we will target thermoelectric energy converters with low-melting ionic liquids (organic salts) as electrolytes, including supercapacitors to make synergy with the work in WP2.  
With an exceptional basis in non-equilibrium thermodynamics we are well set to find and model the transported entropy and reversible heat changes, properties which are decisive in this context. 
%Low-melting ionic electrolytes allow us to deal with low temperature waste heat, which is abundant in the metallurgical and process industry in Norway, in a completely new way.  
In order to engineer new materials for longer device time the reversible heat terms as well as entropy production need be addressed
by these methods.
As a consequence of the introduction of renewable energy, novel electrochemical energy storage devices are currently and continuously proposed. This WP, in synergy with WP2, ensures advanced knowledge on transport properties for engineers.
\newline
