\documentclass[11pt]{ltxdoc}
\newcounter{magicrownumbers}
\newcommand\rownumber{\stepcounter{magicrownumbers}\arabic{magicrownumbers}}
\usepackage{geometry}
\geometry{a4paper, hmargin=2cm, vmargin=2cm}
\usepackage{graphicx}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{tabularx}
\usepackage[table]{xcolor}
\usepackage{color}
\usepackage{hyperref}
\usepackage[superscript,biblabel]{cite}
\usepackage[font=scriptsize,width=14cm]{caption}
\usepackage{wrapfig}
\usepackage{times}
\usepackage{float}
\usepackage{fancyhdr}

\usepackage[compact]{titlesec}
\usepackage[parfill]{parskip}
\usepackage{enumitem}


\pagestyle{fancy}
\fancyhf{}
\fancyhead[LE,RO]{NanoChaT}
\fancyhead[RE,LO]{Center for Nanoscale Charge Transport}
\fancyfoot[C]{\thepage}

\makeatletter
\newcommand\footnoteref[1]{\protected@xdef\@thefnmark{\ref{#1}}\@footnotemark}
\makeatother

% -- main purpose --

\begin{document}
%%%%
%% Main document 12pt or {\large ...}
%%%%
{\large 
\begin{center}
\huge{\bf Center for Nanoscale Charge Transport (NanoChaT)}\\
\begin{center}
    \includegraphics[height=.2 \textwidth]{logoSFF}
\end{center}
\end{center}
\thispagestyle{empty}
\noindent
{\bf Project summary} \\
The transition to a green society will require improved energy
conversion systems and for many of these the fundamental process is
charge transport. A better understanding of charge transport at the
nanoscale will boost development of more efficient energy conversion.
To achieve this, we will develop a theoretical and computational
framework guided by the advice of industry partners and experimental
scientists. The unique feature of the NanoChaT center of excellence
(CoE) is that it will link transport of electrons and ions, which
is currently lacking. This hiatus is due to its cross-disciplinary
character requiring a combination of theoretical understanding
and skills beyond the capacity of an individual. The researchers
connected to NanoChaT have the right complementary knowledge and
proficiency to make an impact. Our method will unify a broad
spectrum of techniques including continuum modeling, chemometrics,
non-equilibrium thermodynamics, atomistic simulations, and electronic
structure theory. For instance, ion transport can either be modeled
macroscopically as charge densities with continuum models and
thermodynamics or on the nanoscale by molecular simulations, while
electron transport can be either described by density functional
theory or more accurate wave function theory. We will use multi-level
methods and chemometrics techniques to connect the different length-
and time-scales, especially at the nanoscale where electron- and
ion-transport influence each other like in electrode reactions. Our
generic approach will be directed towards these selected number of
applications like organic solar cells, supercapacitors, batteries,
fuel cells, thermoelectric energy converters, and artificial
photosynthesis. We have the ambitious goal to establish breakthroughs
in these specific applications, but also by the establishment of our
overall method which should be instrumental for all processes which
involves ion- and electron transport at the nanoscale.
%PO: include a sentence on coupling to heat transfer and applied voltages/electric field
\\ \newline

\noindent
{\bf Primary and secondary objectives of the project} \\ 

Our primary objective is to develop a generic multi-scale approach to
model all aspects of charge transport at the nanoscale involving both
electron- and ion-transport. This model will not be just descriptive,
but should increase our understanding on this process such that it can
generate an avalanche of industrial breakthroughs. We will steer our
theoretical and computational model development in order to address
relevant unresolved questions with respect to specific applications
like organic solar cells and supercapacitors. If successful, our
project will be a unique realization of a multi-scale approach
ranging from highly accurate quantum mechanical calculations up to
the macroscopic limit. Our secondary objectives are related to the
applications. Our modeling will pave the way for the development of
alternative materials for environmental energy production, or will
hint on experimental strategies to increase the efficiency or decrease
aging of present energy devices.

\clearpage
\noindent


{\Large \bf Proposed research} \\
Charge transport at the nanoscale is a fundamental key process in
several nanodevices which play an increasingly important role on
the world energy balance.\footnote{E. Pop, Energy Dissipation and
Transport in Nanoscale Devices, {\em Nano Res} 3, 147-169, (2010).}
Nanotechnology is crucial for the high efficiency conversion of solar
energy or use of solar energy for hydrogen production, and it will
be central in future possibilities for storage and transmission of
energy. The theoretical research on nanoscale processes is making
rapid development, also because the field contains many unexplored
opportunities while the phenomena at this scale is difficult to
address with standard theories. Theories for describing nanosized
objects fall just in between the more established theories for the
atomistic and continuum regions. Finite size effects play an important
role implying that successful approximations like the use of periodic
boundary conditions or analytical tricks based on the translational
symmetry can't and shouldn't be applied; the macroscopic limit does
not apply. On the other hand, the nanoscale exceeds the atomistic
and molecular length scale which can be treated by highly accurate
quantum chemistry methodologies. At the nanoscale the quantum effects
are important, but often not limited to a small atomistic group.
The cooperative effects due to the interactions of multiple atoms
sometimes cause unexpected phenomena\footnote{J. P. Bergfield,
M. A. Ratner, C. A. Stafford, and M. Di Ventra, Tunable quantum
temperature oscillations in graphene nanostructures, {\em Phys. Rev.
B}, 91, 125407, (2015)}. In addition, when a macroscopic number of
nanoparticles interacts also continuum descriptions become essential.
To connect all these length scales in a theoretical framework is
essential since the beauty of nanotechnology shows that quantum
mechanical phenomena at the nanoscale can have an effect at the
macroscopic scale. Especially regarding nanoscale charge transport
there are yet unexplored avenues which lack a solid theoretical
or computational treatment. The development of such a combined
theoretical-computational framework could have an enormous impact
regarding applications just like the theory of semiconductors have
enabled the creation of the transistor which has revolutionized the
field of electronics.

Nanoscale charge transport is especially intriguing from a
theoretical point of view when both electron and ion transport occur
simultaneously and mutually influence each other. The description
of such effects requires the combination of different disciplines
which are seldom available within a single research collaboration.
The availability of a generic theory or computational method will
be invaluable to unravel the detailed functioning of capacitors,
batteries, solar cells, electrochemical converters, and other energy
related nanodevices. Another complexity is that the dynamical nature
of transport processes brings in another dimension which is time.
This implies that besides linking the different length scales,
our method should also be able to link different time scales. For
instance if diffusion occurs via the crossing of different barriers
or if the process involves chemical reactions, there can be a huge
range of relevant timescales ranging from molecular motion to the
barrier crossing event\footnote{C.~Dellago, P.~G. Bolhuis, F.~S.
Csajka, and D.~Chandler, Transition path sampling and the calculation
of rate constants, {\em J. Chem. Phys.}, 108:1964--1977, (1998).}.
In conclusion, the topic of charge transport is timely, highly
challenging, but also highly promising since it may open new fields of
energy related applications.\\

\noindent
{\bf Applications and Workpackages.} The research work will be
structured in four work-packages (WPs) which are categorized according
to application field. However, the methodology we will develop will
stretch across all four WPs. The work-pack leaders (WPL's) are
selected based on their track record in the specific application
field. However, the research within the WP is not a separate entity
dictated by the WPL's research interest. Rather, the WPL will
coordinate the collaboration between the different researchers
and ensure that the cooperative strategy will be applied to the
application field. The WPs listed below involve both electron- and
ion-transport, and a more fundamental understanding on these phenomena
at the nanoscale is expected to create breakthroughs regarding
efficiency and new functionality. The consortium, presented here, can
provide an unique complementary set of methods and theories which are
able to bridge the relevant length- and time-scales and to connect
physically distinct disciplines related to ion-transport on one side
and electron transport on the other side. This proposal is going
to establish that. We give a sketch of our overall methodological
strategic plan in the next paragraph.

At the atomistic scale the highly accurate Coupled Cluster (CC)
framework of models is at our disposal. The CC methods are
computational intensive due to their steep scaling with the size
of the system, but the applicability range can be extended by the
recently developed multi-level CC methods\footnote{R.~H. Myhre,
A.~M. J. Sanchez de Meras, and Henrik Koch, Multi-level coupled
cluster theory, {\em J. Chem. Phys.},141, 224105 (2014).}. However,
the CC methods will be intractable at some point when increasing
the length scale, at which point we will use density functional
theory (DFT). The choice of density functional is generally based
on common practice rather than system specific benchmarking which
is likely to cause a significant drop in accuracy compared to CC.
However, we can use techniques from chemometrics to select the
right functionals to maintain accuracy and predictive power at
the larger length scale\footnote{V. Venkatraman, S. Abburu, B.
K. Alsberg, Can chemometrics be used to guide the selection of
suitable DFT functionals?, {\em Chemom. Intell. Lab. Syst.} 142,
87-94 (2015).}. Also the timescale can be extended, even without
invoking approximations using Transition Interface Sampling
(TIS)\footnote{\label{titus_foot}T. S. van Erp, D.~Moroni, and
P.~G. Bolhuis, A novel path sampling method for the sampling of
rate constants, {\em J. Chem. Phys.}, 118:7762-7774, (2003).}.
This is required if there is for instance a chemical reaction
with a high barrier. This approach also provides a way to connect
classical dynamics with quantum-based simulations via the new QuanTIS
method\footnote{\label{anders_foot} A. Lervik and T. S. van Erp,
Transition path sampling and the calculation of rate constants,{\em
J. Chem. Theory Comput.} 11: 2440-2450 (2015).}. The method
establishes an dynamical analogue of QM/MM in which two levels of
theory are connected in time rather than space. In addition, the
method\footnoteref{anders_foot} can also be used to fit parameters
of reactive force fields\footnote{H. Y. Cheng, Y. A. Zhu, D. Chen,
P. O. Astrand, P. Li, Z. W. Qi, and X.G. Zhou, Evolution of Carbon
Nanofiber-Supported Pt Nanoparticles of Different Particle Sizes: A
Molecular Dynamics Study, {\em J. Phys. Chem. C} 118, 23711-23722
(2014).} on-the-fly (e.g using simulated annealing or evolutionary
algorithms\footnote{\label{alsberg_foot}V. Venkatraman, M. Foscato,
V. R. Jensen, and B. K. Alsberg, Evolutionary de novo design of
phenothiazine derivatives for dye-sensitized solar cells, {\emph
J. Mater. Chem. A} 3, 9851-9860 (2015).}). In this way we can
move from the atomistic and electronic motions to the processes
at the nanoscale. This will allow us to study electron transfer
at the electrodes in combination with ion diffusion. In order
to study the relation between the nanoscale processes and the
macroscopic processes defining e.g. the efficiency of an Li-ion
battery, we will also connect these simulations with non-equilibrium
thermodynamics\footnote{S. Kjelstrup, P. J. S. Vie, L. Akyalcin, P
Zefaniya, J. G. Pharoah, and O. S. Burheim, The Seebeck coefficient
and the Peltier effect in a polymer electrolyte membrane cell with two
hydrogen electrodes, {\em Electrochim. Acta} 99, 166-175 (2013).} and
continuum models of charge transport\footnote{B. A. Grimes, and A. L.
Liapis, The Interplay of Diffusional and Electrophoretic Transport
Mechanisms of Charged Solutes in the Liquid Film Surrounding Charged
Nonporous Adsorbent Particles Employed in Finite Bath Adsorption
Systems, {\em J. Colloid Interf. Sci.}, 248, 504-520 (2002).}. For
instance non-equilibrium molecular dynamics\footnote{F. Roemer, A.
Lervik, F. Bresme, Nonequilibrium molecular dynamics simulations of
the thermal conductivity of water: A systematic investigation of
the SPC/E and TIP4P/2005 models, {\em J. Chem. Phys. } 137, 074503
(2012).} can be used to match parameters of non-equilibrium square
gradient theory for the description of mass and heat transfer at
nano-objects\footnote{\O. Wilhelmsen, T. T. Trinh, S. Kjelstrup, T. S.
van Erp, and D. Bedeaux, Heat and Mass Transfer across Interfaces in
Complex Nanogeometries, {\em Phys. Rev. Lett. } 114, 065901 (2015).}.
This type of multi-scale approach will be applied to all WPs. Though
each WPs has specific modeling problems and objectives that we will
discuss in the following.

%Suggestion of Odne. To make them believe that we will actually collaborate together instead of being inside our own little islands doing 
%our own things
%\emph{Quarterly meetings will be held with all participants outside the university campus in order to stimulate collaborative efforts between researchers from different disciplines.}

 

 
%{\bf Some words on multi-level methods, QuanTIS, force field development etc}


\textbf{WP1:  Organic solar cells. WPL:  Alsberg/van Erp.}\\
%PO Organic solar cells and DSSC are not the same thing!
  There is an intense research activity worldwide to develop
  alternatives to silicon-based solar technologies. One of the most
  promising candidates is dye-sensitized solar cells (DSSCs, also
  termed Gr\"atzel cells) which are much cheaper to manufacture, have a
  smaller carbon footprint and are more flexible than silicon-based
  solar cells. However, their solar-to-electrical energy conversion
  efficiency seems to have stagnated at 11-13\%, compared with up to
  25\% for crystalline-silicon devices. Central to improving the
  performance of DSSCs is the design of better dye molecules, in
  particular metal-free organic dyes.  In a recent
  article\footnoteref{alsberg_foot} evolutionary {\em de novo} design was presented, wherein
  molecular structures were systematically assembled and iteratively
  refined in a computer according to the principles of evolution.
  Central to this scheme is the computation of the power conversion
  efficiency (PCE) of the solar cell which is to be optimised. Owing
  to the complex and multiscale nature of the device morphology, the
  PCE (and other properties) cannot as yet be computed directly using
  quantum chemistry methods. Instead, the prediction of PCE and other
  property values is performed using machine learning models based on
  data taken from either the literature, provided by experimental
  collaborators or from costly computations. The PCE may be reduced by
  various physical phenomena in the solar cell such as increased dye
  aggregation. This reduces the PCE due to intermolecular excited
  state quenching. The interaction of dye molecules bonded to the
  semiconductor surface (such as TiO$_2$) can be studied by molecular
  dynamics simulations using either force fields or DFT methods. The
  aim of the simulations will be to understand and predict the degree
  of dye aggregation to accomplish efficient screening of suitable DSSC
  dyes.


%\item[WP1:] {\bf Organic solar cells.} WPL:  Alsberg/van Erp.\\
% There is an intense research activity worldwide to develop
%  alternatives to silicon-based solar technologies. One of the most
%  promising candidates is dye-sensitized solar cells (DSSCs, also
%  termed Grätzel cells) which are much cheaper to manufacture, have a
%  smaller carbon footprint and are more flexible than silicon-based
%  solar cells. However, their solar-to-electrical energy conversion
%  efficiency seems to have stagnated at 10–12\%, compared with up to
%  25\% for crystalline-silicon devices. Central to improving the
%  performance of DSSCs is the design of better dye molecules, in
%  particular metal-free organic dyes.  In a recent
%  article~\cite{Alsberg3}
%  from the Alsberg group at ATK, a new approach based on {\em in
%    silico} evolutionary {\em de novo} design was presented, wherein
%  molecular structures were systematically assembled and iteratively
%  refined in a computer according to the principles of evolution.
%  Central to this scheme is the computation of the power conversion
%  efficiency (PCE) which is to be optimised. Other properties of the
%  dye molecules are also of interest such as long term stability,
%  toxicity and various optical properties. Owing to the complex and
%  multiscale nature of the device morphology, the PCE (and other
%  properties) cannot as yet be computed directly using quantum
%  chemistry methods. For those properties which can be computed, the
%  computational cost may be too high to be used for high throughput
%  screening which is needed in an evolutionary {\em de novo} design
%  approach where hundreds or thousands of structures are
%  examined. Thus, an important alternative approach being used is
%  employing a quantitative structure-property relationship (QSPR)
%  model which is built using data taken from either the literature,
%  provided by experimental collaborators or from costly
%  computations.
%  % A predictive machine learning/chemometric model is
%  %created that correlates computationally inexpensive structure
%  %descriptors calculated for each dye (such as computed vibrational
% % frequencies and molecular orbital energies) and the reported
%  %property to be optimised (such as the PCE or the stability). 
%  The
%  established QSPR model is exploited in the evolutionary setup to
%  steer the molecular design towards novel structures with
%    improved property values, while excluding unpromising ones.
%In cases of insufficient experimental data for
%  building predictive QSPR models, investigations will be more reliant
%  on theoretical calculations. Of particular interest is to explore
%  whether the our evolutionary {\em de novo} design method can use
%  fitness values based on results from quantum chemical computations of 
%  the photoexcited dynamics of dye molecules.
%


\textbf{WP2: { Chemical reactions in Electric fields.} WPL: \AA strand/Grimes.}\\
Many chemical processes of relevance for this project occur in high
electric fields (in an applied voltage) as for example in batteries,
solar cells, fuel cells and electrically insulating materials.
Although in principle the same computational methods apply as for the
isolated system, additional challenges arise for modeling chemical
processes in an electric field. For example, the electronic ground
state may not be the same anymore and the ionization energy is much
lower in an electric field limiting the number of available excited
states. In addition, energy is continuously added to a system in an
applied voltage, which has to be balanced by emission of heat or light
to reach a steady-state condition, so equilibrium methods do not apply
for a system in an electric field. In this WP, we will validate and
develop molecular modeling tools applicable for studying molecular
systems in an electric field including quantum chemical response
properties as excitation energies, electron transport in a dielectric
material as well as ion transport in condensed media.
 

\textbf{WP3: {\bf Photochemical energy conversion} WPL: H\o yvik/Koch}\\
Molecules can convert sunlight into different forms of energy, for
example changing chemical bonds, or converting the absorbed energy
to heat without molecular degradation, e.g., through a concerted
motion of electrons and nuclei in the excited state. Such processes
are highly relevant to be able to exploit photochemical energy
conversion, but the processes are challenging to understand from
experiments alone. Per today, methods which provides essential
accuracy for molecular structure and excitation energies, fail to
describe photoexcited dynamics. Further, the optimal theoretical
framework for describing excited state properties assumes that the
molecule is initially in the ground state. In this WP, we will develop
new theoretical models, in the coupled cluster framework, that enables
a description of photoexcited dynamics and properties of molecules
in excited states. Some of the excited state properties are local in
nature, and for these methods we will also implement the developed
theory in a multi-level framework, to obtain computational savings.
The WP will be carried out in collaboration with experimentalist at
SLAC National Accelerator Laboratory at Stanford University.




\textbf{WP4: {\bf Electrochemical energy conversion.} WPL: Schnell/Burheim} \\
Electrical energy is produced from sources such as light (solar
cells), waste heat (thermoelectricity) or hydro power. Electrical
energy has to be used immediately or stored in a different form,
e.g. like chemical energy in a battery. Most sources of renewable
electrical energy are intermittent. A society based on renewable
energy technology, \textit{e.g.} 70-60\% solar or wind energy, will
be strongly dependent on batteries, fuel cells, thermoelectric
converters, supercapacitors, and advanced flow-cell batteries to store
electrical energy until it is needed. In all these technologies the
energy is stored electrochemically.

In this work package we will investigate the electrode reversible
heat, the half cell\footnote{ S.K. Schnell, P Englebienne, JM Simon,
P Kr\"uger, SP Balaji, S Kjelstrup, ``How to apply the Kirkwood-Buff
theory to individual species in salt solutions'', Chemical Physics
Letters 582 (2013) 154-157.} reaction entropy and transported entropy
in the electrolytes of such systems.\footnote{S. Kjelstrup, P.J.S.
Vie, L. Akyalcin, P. Zefaniya, J.G. Pharoah, O.S. Burheim, ``The
Seebeck coefficient and the Peltier effect in a polymer electrolyte
membrane cell with two hydrogen electrodes'', Electrochimica Acta,
99 (2013) 166–175.} These properties are important for managing heat
in energy storage systems \footnote{ O.S. Burheim, M. A. Onsrud,
J.G. Pharoah, F. Vullum-Bruer, P.J.S. Vie, ``Thermal Conductivity,
Heat Sources and Temperature Profiles of Li-ion Batteries'', ECS
Transactions, 58 (48) (2014) 145-171.}, to improve the lifetime and
efficiency of such systems. \footnote{O.S. Burheim, M. Aslan,
J.S. Atchison, V. Presser, ``Thermal Conductivity and Temperature
Profiles in Carbon Electrodes for Supercapacitors'', J. Power Sources,
246 (2014) 160-166.}. Moreover, energy storage applications for the
automotive industry emerge in the best laboratories around the world
are made of advanced inorganic metal oxides. The workpackage leaders
holds excellent competence in determining thermodynamic properties and
international networks for having early access to these materials -
intended for use in batteries and supercapacitors\footnote{McDonald,
Mason, Kong, Bloch, Dani, Crocellà, Giordanino, Odoh, Drisdell,
Vlaisavljevich, Dzubak, Poloni, Schnell, Planas, Lee, Pascal,
Wan, Prendergast, Neaton, Smit, Kortright, Gagliardi, Bordiga,
Reimer, Long, ``Cooperative insertion of CO$_2$ in diamine-appended
metal-organic frameworks'', \textit{Nature} 519 (2015) 303-308}. By
include quantum chemistry, density function theory, and molecular
dynamics expertise of Koch, {\AA}str{\o}m, van Erp, and H{\o}yvik,
this WP will allow us to understand electrochemical energy on a new
level, and can guide the construction of new and better electrochemical
energy storage systems.

\noindent
{\Large \bf Organization of the center} \\ 
There are eight scientists in the NanoChaT collaboration where six
are employed at NTNU and two at HiST. In 2016 the two universities
will merge and all members will relocate to the CoE's common physical
premises at the Department of Chemistry (IKJ) at the Faculty of
Science and Technology (NT-faculty). As detailed in Table 1, the group
covers a broad range of competences in theoretical and computational
chemistry, as well as experimental electrochemical energy conversion.
We give priority to early career researchers when selecting the six
(the upper limit) principal investigators. This will provide Schnell
and H\o yvik with unique career opportunities as WP leaders and they
will be assisted by Burheim and Koch.



\begin{table}[H]
\centering
\caption{The NanoChaT collaboration.}
\includegraphics[width=14cm]{table_grey2.png}
\end{table}

\noindent
A management team, consisting of the Director and Vice Director
together with IKJ Head of Department Marie-Laure Olivier as
administrative leader, is appointed and will be responsible for the
daily operation of the center. An administrative secretary will be
employed by the center whom will participate in the leader team
meetings. The Vice Director acts as advisor to the Director on a daily
basis, and takes on the responsibilities of Director in the absence
of the latter. Director and Vice Director will switch roles after the
5 year mid-term of the CoE. A council of all investigators will meet
once a week to discuss and plan research projects.

\noindent
The CoE will have an Executive Board which oversees that the CoE is
operating according to its mandate. The Executive Board will also
acts as an ambassador for the CoE, and is responsible with respect
to the institutions financing it: NTNU and RCN. The Director acts as
secretary, and does not have voting rights on the Executive Board. The
Dean of the NT-faculty (Prof. Anne Borg) is chairman of the Board.
Further, the CoE will have a Scientific Advisory Board with members
composed of academics (primarily experimentalists), researchers and
industrial representatives who have interest in the research topics of
the CoE and understand the practical benefits of the results produced
by the CoE. The detailed composition of the Boards will given in phase
2.


The Scientific Advisory Board members will be invited once a year to
CoE seminars to review the current status of the research and offer
advice and opinions on how to guide the fundamental research moving
forward based on what they perceive as practical benefits of the CoE's
research. The Scientific Advisory Board reports to the Executive Board
on the scientific work and output of the CoE. It meets once a year
with the CoE and on the basis of this meeting writes a report to the
Executive Board. The report concludes with recommendations on the
scientific direction of the CoE.

%People that we will invite for the Advisory Board and have confirmed their interest are Assoc. Prof. B\aa rd Hoff, Assoc. Prof. Odd Gautun, and  Prof. Anne Fiksdahl(organic chemistry, IKJ), Preben Vie (IFE, Institute for Energy Technology), Oddmund Mallevik (former employee Statoil, division electrolysis ??).

\noindent
To strengthen the embedding of our research within international      
collaborations we will open five Professor II positions (each with    
a duration of 4 years) which will complement the three existing       
Professor II's associated with the center (see Table \ref{profII}).   
This will give added value of creating the CoE, since the Professor   
II's will benefit NTNU and Norway through their international         
networks and competences. The CoE will also give added value in terms 
of                                                                    
\begin{itemize}
\item training highly competent candidates for the industry, academia,
or public sector.
\item generating a platform of knowledge on charge transport processes
on a multi-level scale which stretches across several disciplines at
the NT-faculty and which integrates researchers at the NTNU fusion
partner HiST.
\item motivating students by creating a visible and strong link
between courses in disciplines involved in the CoE (quantum
chemistry, thermodynamics, molecular modeling, transport processes,
electrochemistry).
\end{itemize}


\begin{table}[H]
\centering
\caption{\label{profII} Overview of prospective and current (marked with an asterisk) Professor II's.}
\includegraphics[width=14cm]{assoc_inv.png}
\end{table}

\noindent
When consolidating scientists from several different research fields,
it is important to achieve good communication across disciplines.
This is needed to establish the cross-links between the different
methodologies and to establish the feeling to be part of a team.
Therefore, CoE seminars will be organized four times per year. At
least two of these four involve overnight stays off campus to increase
the coherency of the CoE team and to enforce the bridge building and
brain storming sessions without the daily duties and distractions
that all researchers experience in their normal work environment.
The CoE seminars contain presentations by PhD students and postdocs
showing their latest preliminary results, prospectives, and potential
problems and obstacles that need to be overcome. PI's will present
more overview talks and long-term plans. In addition, their should be
ample time for discussions.


\end{document}




